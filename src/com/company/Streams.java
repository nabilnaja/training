package com.company;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.summarizingInt;

public class Streams {

    public static void main(String[] args) {
	// write your code here
        List<String> mots = Arrays.asList("Giant Guerilla", "Punk IPA", "Chimay Grand Reserve", "Double Chocolate", "Ruination", "Malt Capone", "Hel & Verdoemenis");

        //Retourne une liste avec le nombre de lettres de chacune des entrées
        // [14, 8, 20, 16, 9, 11, 17]
        System.out.println("Retourne une liste avec le nombre de lettres de chacune des entrées");
        System.out.println(
                mots.stream()
                        .map(String::length)
                        .collect(Collectors.toList())
        );

        //Retourne une liste avec le nombre de lettres de chacune des entrées triées dans l'ordre alphabétique
        // [20, 16, 14, 17, 11, 8, 9]
        System.out.println("Retourne une liste avec le nombre de lettres de chacune des entrées triées dans l'ordre alphabétique");
        System.out.println(
                mots.stream()
                        .sorted()
                        .map(String::length)
                        .collect(Collectors.toList())
        );

        //Retourne le nombre de lettres total de toutes les entrées   95
        System.out.println("Retourne le nombre de lettres total de toutes les entrées");
        System.out.println(
                mots.stream()
                        .mapToInt(String::length)
                        .sum()
        );

        //Retourne le nombre de lettres de toutes les entrées qui contiennent la lettre "C" (majsucule)  47
        System.out.println("Retourne le nombre de lettres de toutes les entrées qui contiennent la lettre C (majsucule)");
        System.out.println(
                mots.stream()
                        .filter(s -> s.contains("C"))
                        .mapToInt(String::length)
                        .sum()
        );

        //Retourne l'entrée la plus longue de la liste    Chimay Grand Reserve
        System.out.println("Retourne l'entrée la plus longue de la liste");
        System.out.println(
                mots.stream()
                        .max(Comparator.comparingInt(String::length))
                        .get()
        );

        //Retourne le mot le plus long de la liste  Verdoemenis
        System.out.println("Retourne le mot le plus long de la liste ");
        System.out.println(
                mots.stream()
                        .flatMap(s -> Stream.of(s.split(" ")))
                        .max(Comparator.comparingInt(String::length))
                        .get());

        //Retourne un string concatenant tous les mots de plus de 5 lettres de la liste
        // GiantGuerillaChimayGrandReserveDoubleChocolateRuinationCaponeVerdoemenis
        System.out.println("Retourne un string concatenant tous les mots de plus de 5 lettres de la liste");
        System.out.println(
                mots.stream()
                        .flatMap(s -> Stream.of(s.split(" ")))
                        .filter(s -> s.length() >= 5)
                        .collect(Collectors.joining()));
    }
}
